<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = array("product_id", "quantity", "unit_price", "total_price");
}
