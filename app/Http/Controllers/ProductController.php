<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Invoice;
use App\Product;
use App\Sales;
use DataTables\Database;
use DataTables\Editor;
use DataTables\Editor\Field;
use DataTables\Editor\Options;
use DataTables\Editor\Validate;
use DataTables\Editor\ValidateOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;


class ProductController extends Controller
{
    public function index()
    {
        return View::make("product.list");
    }

    public function purchase()
    {
        return View::make("product.purchase");
    }

    public function purchase_data(Request $request)
    {
        $db = new Database(array(
            "type" => "Mysql",
            "pdo" => DB::connection()->getPdo()
        ));
        Editor::inst($db, 'carts', "id")
            ->fields(
                Field::inst('products.name'),
                Field::inst('carts.quantity'),
                Field::inst('carts.unit_price'),
                Field::inst('product_purchases.product_id')
                    ->options(Options::inst()
                        ->table('products')
                        ->value('id')
                        ->label('name')
                    )
            )
            ->leftJoin("products", "carts.product_id", "=", "products.id")
            ->process($_POST)
            ->debug(true)
            ->json();
    }

    public function sales()
    {
        return View::make("product.sales");
    }

    public function sales_data(Request $request)
    {
        $db = new Database(array(
            "type" => "Mysql",
            "pdo" => DB::connection()->getPdo()
        ));
        Editor::inst($db, 'carts')
            ->fields(
                Field::inst('products.name'),
                Field::inst('carts.quantity'),
                Field::inst('carts.id'),
                Field::inst('carts.unit_price'),
                Field::inst('carts.total_price'),
                Field::inst('carts.product_id')
                    ->options(Options::inst()
                        ->table('products')
                        ->value('id')
                        ->label('name')
                    )
            )
            ->on('preEdit', function ($editor, $id, $values) {
                $cart = Cart::find($id);
                $qty = isset($values['carts']['quantity']) ? $values['carts']['quantity'] : $cart->quantity;
                $price = isset($values['carts']['unit_price']) ? $values['carts']['unit_price'] : $cart->unit_price;
                $editor
                    ->field('carts.total_price')
                    ->setValue($qty * $price);
            })
            ->leftJoin("products", "carts.product_id", "=", "products.id")
            ->process($_POST)
            ->debug(true)
            ->json();
    }

    public function list(Request $request)
    {
        if (isset($request->search['value'])) {
            $product = Product::where("name", "like", "%" . $request->search['value'] . "%")->get();
        } else {
            $product = Product::all();
        }
        $data = array();
        foreach ($product as $p) {
            $data[] = array(
                "DT_RowId" => $p->id,
                "name" => $p->name,
                "code" => $p->code,
                "stock" => $p->stock,
                "sales_price" => $p->sales_price,
            );
        }
        return response()->json(array("data" => $data));
    }

    public function new(Request $request)
    {
        // return response()->json($_POST);
        $db = new Database(array(
            "type" => "Mysql",
            "pdo" => DB::connection()->getPdo()
        ));
        Editor::inst($db, 'products')
            ->fields(
                Field::inst('user_id')->setValue(Auth::user()->id),
                Field::inst('stock'),
                Field::inst('sales_price')
                    ->validator(Validate::numeric()),
                Field::inst('name')
                    ->validator(Validate::notEmpty(ValidateOptions::inst()
                        ->message('Nama Produk harus diisi')
                    )),
                Field::inst('code')
            )
            ->process($_POST)
            ->debug(true)
            ->json();
    }

    public function cart_remove(Request $request)
    {
        $cart = Cart::find($request->id);
        $del = $cart->delete();
        if ($del) {
            return 1;
        } else {
            return "gagal menghapus data";
        }
    }

    public function list_data(Request $request)
    {
        $product = Product::where("user_id", $request->user()->id)
            ->where("name", "like", $request->term . "%")
            ->get();
        return response()->json($product);
    }

    public function cart(Request $request)
    {
        if (empty($request->pid)) exit("Silahkan pilih produk");
        if (empty($request->qty)) exit("Silahkan isi quantity produk");
        if (empty($request->price)) exit("Silahkan isi harga produk");
        $data = array(
            "product_id" => $request->pid
        );
        $cart = Cart::firstOrNew($data);
        $cart->quantity = $request->qty;
        $cart->unit_price = $request->price;
        $cart->total_price = $request->price * $request->qty;
        $cart->user_id = $request->user()->id;
        $saved = $cart->save();
        if ($saved) {
            return 1;
        } else {

            return "gagal menyimpan data";
        }
    }

    public function new_invoice(Request $request)
    {
        $rules = array(
            'buyer' => 'required|string|min:3|max:100',
            'ptotal' => 'required|integer|min:1'
        );
        $message = array(
            "required" => ":attribute harus diisi",
            "min" => ":attribute harus diisi minimal :min karakter"
        );
        $validator = Validator::make(Input::all(), $rules, $message);
        if ($validator->fails()) {
            $err = $validator->getMessageBag()->toArray();
            foreach ($err as $e => $v) {
                $err[] = $v;
            }
            return response()->json(array("err" => $err));
        } else {
            $data = array(
                "user_id" => $request->user()->id,
                "buyer_name" => $request->buyer,
                "discount_percentage" => $request->disc_percent,
                "discount_price" => $request->disc_price,
                "discounted_price" => $request->ptotal - $request->disc_price,
                "total_price" => $request->ptotal,
            );
            $invoice = Invoice::create($data);
            $carts = Cart::where("user_id", $request->user()->id)->get();
            foreach ($carts as $cart) {
                $sales = array(
                    "invoice_id" => $invoice->id,
                    "product_id" => $cart->product_id,
                    "quantity" => $cart->quantity,
                    "unit_price" => $cart->unit_price,
                    "total_price" => $cart->total_price
                );
                Sales::create($sales);
                $cart->delete();
            }
            return 1;
        }
    }

    public function invoice_list(Request $request)
    {
        $db = new Database(array(
            "type" => "Mysql",
            "pdo" => DB::connection()->getPdo()
        ));
        Editor::inst($db, 'invoices')
            ->fields(
                Field::inst('buyer_name'),
                Field::inst('discount_percentage'),
                Field::inst('discount_price'),
                Field::inst('discounted_price'),
                Field::inst('total_price'),
                Field::inst('id')
            )
            ->on('preEdit', function ($editor, $id, $values) {
                $invoice = Invoice::find($id);
                $dis = isset($values['discount_percentage']) ? $values['discount_percentage'] : $invoice->discount_percentage;
                $disp = isset($values['discount_price']) ? $values['discount_price'] : $invoice->discount_price;
                $total_price = $invoice->total_price;
                $disprice = $total_price / 100 * $dis;
                $discounted_price = $total_price - $disprice;
                if (isset($values['discount_percentage'])) {
                    $editor
                        ->field('discount_price')
                        ->setValue($disprice);
                    $editor
                        ->field('discounted_price')
                        ->setValue($discounted_price);
                }
                if (isset($values['discount_price'])) {
                    $editor
                        ->field('discount_percentage')
                        ->setValue(number_format($disp / $total_price * 100, 2));
                    $editor
                        ->field('discounted_price')
                        ->setValue($total_price - $disp);
                }

            })
            ->process($_POST)
            ->debug(true)
            ->json();
    }
}
