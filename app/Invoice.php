<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = array("user_id", "buyer_name", "discount_percentage", "discount_price", "total_price");
}
