<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = array("user_id", "code", "name", "sales_price", "stock");
}
