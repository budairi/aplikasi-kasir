<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = "product_sales";
    protected $fillable = array("invoice_id", "product_id", "quantity", "unit_price", "total_price");
}
