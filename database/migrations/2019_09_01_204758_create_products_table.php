<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("user_id");
            $table->string("code", 100)->nullable();
            $table->string("name", 100);
            $table->unsignedBigInteger("sales_price")->default(0);
            $table->unsignedInteger("stock")->default(0);
            $table->timestamps();
            $table->foreign("user_id")->references("id")->on("users")->onUpdate("cascade")->onDelete("cascade");
            $table->softDeletes();
        });
        Schema::create('product_purchases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("product_id");
            $table->unsignedInteger("quantity")->default(0);
            $table->unsignedBigInteger("unit_price")->default(0);
            $table->unsignedBigInteger("total_price")->default(0);
            $table->timestamps();
            $table->foreign("product_id")->references("id")->on("products")->onUpdate("cascade")->onDelete("cascade");
            $table->softDeletes();
        });
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("user_id");
            $table->string("buyer_name", 50);
            $table->double("discount_percentage")->default(0);
            $table->unsignedBigInteger("discount_price")->default(0);
            $table->unsignedBigInteger("discounted_price")->default(0);
            $table->unsignedBigInteger("total_price")->default(0);
            $table->timestamps();
            $table->foreign("user_id")->references("id")->on("users")->onUpdate("cascade")->onDelete("cascade");
            $table->softDeletes();
        });
        Schema::create('product_sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("invoice_id");
            $table->unsignedBigInteger("product_id");
            $table->unsignedInteger("quantity")->default(0);
            $table->unsignedBigInteger("unit_price")->default(0);
            $table->unsignedBigInteger("total_price")->default(0);
            $table->timestamps();
            $table->foreign("product_id")->references("id")->on("products")->onUpdate("cascade")->onDelete("cascade");
            $table->foreign("invoice_id")->references("id")->on("invoices")->onUpdate("cascade")->onDelete("cascade");
            $table->softDeletes();
        });
        Schema::create('carts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("product_id");
            $table->unsignedInteger("quantity")->default(0);
            $table->unsignedBigInteger("unit_price")->default(0);
            $table->unsignedBigInteger("total_price")->default(0);
            $table->timestamps();
            $table->foreign("product_id")->references("id")->on("products")->onUpdate("cascade")->onDelete("cascade");
            $table->foreign("user_id")->references("id")->on("users")->onUpdate("cascade")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_purchases');
        Schema::dropIfExists('product_sales');
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('carts');
        Schema::dropIfExists('products');
    }
}
