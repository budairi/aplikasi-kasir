<?php

use App\Product;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create("id_ID");
        for ($i = 1000; $i < 1050; $i++) {
            Product::create(array("user_id" => 6, "code" => $faker->ean13, "name" => $faker->domainName, "sales_price" => $faker->numberBetween(1000, 130000), "stock" => 0));
        }
    }
}
