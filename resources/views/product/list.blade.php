@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Daftar Produk</div>

                    <div class="card-body">
                        <table id="product" class="table table-bordered">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Nama</th>
                                <th>Kode</th>
                                <th>Stok</th>
                                <th>Harga Jual</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Samphoo</td>
                                <td>5555</td>
                                <td>20</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var editor;
        $(function () {
            editor = new $.fn.dataTable.Editor({
                ajax: "{{route("product.new")}}",
                table: "#product",
                fields: [
                    {label: "Kode Produk:", name: "code"},
                    {label: "Nama Produk:", name: "name"},
                    {label: "Stok Produk:", name: "stock", def: 0},
                    {label: "Harga Jual:", name: "sales_price", def: 0},
                ]
            })
            $('#product').on('click', 'tbody td:not(:first-child)', function (e) {
                editor.inline(this);
            });
            $("#product").DataTable({
                dom: "Bfrtip",
                ajax: "{{route("product.list.json")}}",
                order: [[1, 'asc']],
                columns: [
                    {
                        data: null,
                        defaultContent: '',
                        className: 'select-checkbox',
                        orderable: false
                    },
                    {data: "name"},
                    {data: "code"},
                    {data: "stock"},
                    {data: "sales_price"},
                ],
                buttons: [
                    {extend: "create", editor: editor},
                    {extend: "edit", editor: editor},
                    {extend: "remove", editor: editor}
                ],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
            });
        })
    </script>
@endsection
