@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Pembelian Produk</div>

                    <div class="card-body">
                        <table id="product" class="table table-bordered">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Nama</th>
                                <th>Kuantitas</th>
                                <th>Harga Satuan</th>
                                <th>Harga Total</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var editor;
        var qty = 0, price = 0, total_price = 0;
        $(function () {
            editor = new $.fn.dataTable.Editor({
                ajax: "{{route("product.purchase.data")}}",
                table: "#product",
                fields: [
                    {
                        label: "Nama Produk",
                        name: "product_purchases.product_id",
                        type: "select",
                        placeholder: "Pilih produk"
                    },
                    {label: "Quantitas:", name: "product_purchases.quantity"},
                    {label: "Harga Satuan:", name: "product_purchases.unit_price"},
                    {label: "Harga Total:", name: "product_purchases.total_price", def: 1},
                ]
            })
            $('#product').on('click', 'tbody td:not(:first-child, :nth-child(2))', function (e) {
                editor.inline(this);
            });
            $("#product").DataTable({
                dom: "Bfrtip",
                ajax:
                    {
                        url: "{{route("product.purchase.data")}}",
                        type: "post"
                    },
                order: [[1, 'asc']],
                columns: [
                    {
                        data: null,
                        defaultContent: '',
                        className: 'select-checkbox',
                        orderable: false
                    },
                    {data: "products.name"},
                    {data: "product_purchases.quantity"},
                    {data: "product_purchases.unit_price"},
                    {data: "product_purchases.total_price"},
                ],
                buttons: [
                    {extend: "create", editor: editor},
                    {extend: "edit", editor: editor},
                    {extend: "remove", editor: editor}
                ],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
            });
            $('input', editor.field('product_purchases.quantity').node()).on('keyup', function () {
                qty = $(this).val();
                total_price = qty * price;
                $('input', editor.field('product_purchases.total_price').node()).val(total_price)

            });
            $('input', editor.field('product_purchases.unit_price').node()).on('keyup', function () {
                price = $(this).val();
                total_price = qty * price;
                $('input', editor.field('product_purchases.total_price').node()).val(total_price)
            });
        })
    </script>
@endsection
