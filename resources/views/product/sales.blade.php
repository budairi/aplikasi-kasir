@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <span id="price-label">Rp.0</span>
                        <div class="float-right">
                            <button id="create_invoice" class="btn btn-success">Simpan</button>
                        </div>
                    </div>

                    <div class="card-body">

                        <form class="form-horizontal" role="form">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="inputType" class="col-md-2 control-label">Pembeli</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="inputType" name="buyer"
                                               placeholder="Nama pembeli perorangan atau instansi">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="name" class="col-md-2 control-label">Produk</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="name" name="name"
                                               placeholder="Nama produk">
                                        <input type="hidden" id="pid" name="pid">
                                        <input type="hidden" id="ptotal" name="ptotal">
                                        <input type="hidden" id="disc-percent" name="disc_percent">
                                        <input type="hidden" id="disc-price" name="disc_price">
                                    </div>
                                    <label for="qty" class="col-md-1 control-label">QTY</label>
                                    <div class="col-md-1">
                                        <input min="1" type="number" class="form-control" id="qty" name="qty" value="1">
                                    </div>
                                    <label for="price" class="col-md-1 control-label">Harga</label>
                                    <div class="col-md-2">
                                        <input min="1" type="number" class="form-control" id="price" name="price">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="submit" class="btn btn-info" id="submit" value="Add">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div style="margin-bottom: 10px;"></div>
                        <table style="width: 100%" id="product" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Kuantitas</th>
                                <th>Harga Satuan</th>
                                <th>Subtotal</th>
                                <th width="30px"></th>
                            </tr>
                            </thead>
                        </table>
                        <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-inline float-right">
                                        <label>Total Harga: </label>
                                        <input readonly type="text" id="total_price" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-inline float-right">
                                        <label>Diskon(%): </label>
                                        <input value="0" min="0" type="number" id="discount" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-inline float-right">
                                        <label>Total Bayar: </label>
                                        <input readonly type="text" id="total_pay" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 10px;" class="col-md-12">
                <div class="card">
                    <div class="card-header">Invoice</div>

                    <div class="card-body">
                        <table style="width: 100%" id="table-invoice" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Pembeli</th>
                                <th>Diskon (%)</th>
                                <th>Diskon (Rp)</th>
                                <th>Harga Diskon</th>
                                <th>Total Harga</th>
                                <th width="30px"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var editor, editor_invoice;
        var table, table_invoice;
        var qty = 1, price = 0, total_price = 0;
        var formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
        });
        $(function () {
            editor = new $.fn.dataTable.Editor({
                ajax: "{{route("product.sales.data")}}",
                table: "#product",
                fields: [
                    {
                        label: "Nama Produk",
                        name: "carts.product_id",
                        type: "select",
                        placeholder: "Pilih produk"
                    },
                    {label: "Quantitas:", name: "carts.quantity", def: 1},
                    {label: "Harga Satuan:", name: "carts.unit_price"},
                    {label: "Harga Total:", name: "carts.total_price"},
                ]
            })
            $('#product').on('click', 'tbody td:not(:first-child, :nth-child(4), :nth-child(5))', function (e) {
                editor.inline(this);
            });
            table = $("#product").DataTable({
                ajax:
                    {
                        url: "{{route("product.sales.data")}}",
                        type: "post"
                    },
                columns: [
                    {data: "products.name"},
                    {data: "carts.quantity"},
                    {data: "carts.unit_price"},
                    {data: "carts.total_price", render: $.fn.dataTable.render.number(',', '.', 2, 'Rp')},
                    {data: null, defaultContent: "<button class='bg-danger'>X</button>"},
                    {data: "carts.id", visible: false},
                ],
                columnDefs: [],
                buttons: [
                    {extend: "create", editor: editor},
                    {extend: "edit", editor: editor},
                    {extend: "remove", editor: editor}
                ],
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(3)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(3, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(3).footer()).html(pageTotal);
                    $("#total_price").val(pageTotal);
                    $("#ptotal").val(pageTotal);
                    var dis = $("#discount").val()
                    var disp = pageTotal / 100 * dis
                    $("#total_pay").val(pageTotal - disp)
                    $("#price-label").html(formatter.format(pageTotal - disp))
                    $("#disc-percent").val(dis)
                    $("#disc-price").val(disp)
                },
                paging: false,
                ordering: false,
                searching: false,
                info: false
            });

            $("#name").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "{{route("product.list.data")}}",
                        dataType: "json",
                        data: {term: request.term},
                        success: function (data) {
                            response($.map(data, function (v, i) {
                                return {
                                    label: v.name,
                                    value: v.name,
                                    id: v.id,
                                    price: v.sales_price
                                };
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    $("#pid").val(ui.item.id)
                    $("#price").val(ui.item.price)
                }
            });

            $("form").submit(function (e) {
                e.preventDefault()
                var data = $(this).serialize();
                var btn = $("#submit")
                btn.val("memrosess")
                $.ajax({
                    url: "{{route("product.sales.cart")}}",
                    data: data,
                    method: "post",
                    success: function (r) {
                        if (r == "1") {
                            table.ajax.reload();
                            $("#name, #pid, #price").val("")
                            $("#qty").val(1)
                        } else {
                            alert(r)
                        }
                        btn.val("Add")
                    }
                })
            })

            $("#discount").keyup(function () {
                var pageTotal = $("#total_price").val();
                var dis = $("#discount").val()
                if (dis > 100) {
                    $("#discount").val(0)
                    dis = 0;
                }
                var disp = pageTotal / 100 * dis
                $("#total_pay").val(pageTotal - disp)
                $("#price-label").html(formatter.format(pageTotal - disp))
                $("#disc-percent").val(dis)
                $("#disc-price").val(disp)
            })

            $('#product tbody').on('click', 'button', function () {
                var data = table.row($(this).parents('tr')).data();
                var btn = $(this)
                btn.html("<i class='fa fa-spin fa-spinner'></i>")
                btn.removeClass("danger")
                btn.addClass("warning")
                btn.attr("disabled", "disabled")
                $.ajax({
                    url: "{{route("product.sales.cart.remove")}}",
                    method: "post",
                    data: {id: data.carts.id},
                    success: function (r) {
                        if (r == 1) {
                            table.ajax.reload()
                        } else {
                            alert(r)
                        }
                        btn.html("X")
                        btn.removeClass("warning")
                        btn.addClass("danger")
                        btn.removeAttr("disabled")
                    }
                })
            });

            $("#create_invoice").click(function () {
                var btn = $(this)
                btn.html("<i class='fa fa-spin fa-spinner'></i> menyimpan...")
                $.ajax({
                    url: "{{route("product.invoice.new")}}",
                    method: "post",
                    data: $("form").serialize(),
                    success: function (r) {
                        if (r == 1) {
                            table.ajax.reload()
                            $("form").trigger('reset')
                            table_invoice.ajax.reload()
                        } else {
                            alert(r.err[0])
                        }
                        btn.text('Simpan')
                    }
                })
            })
            editor_invoice = new $.fn.dataTable.Editor({
                ajax: "{{route("product.invoice.data")}}",
                table: "#table-invoice",
                fields: [
                    {label: "Nama Pembeli:", name: "buyer_name"},
                    {label: "Diskon(%):", name: "discount_percentage"},
                    {label: "Diskon(Rp):", name: "discount_price"},
                    {label: "Harga Total:", name: "total_price"},
                ]
            })
            $('#table-invoice').on('click', 'tbody td:not(:nth-child(4), :nth-child(5), :nth-child(6))', function (e) {
                editor_invoice.inline(this);
            });
            table_invoice = $("#table-invoice").DataTable({
                ajax:
                    {
                        url: "{{route("product.invoice.data")}}",
                        type: "post"
                    },
                columns: [
                    {data: "buyer_name"},
                    {data: "discount_percentage"},
                    {data: "discount_price"},
                    {data: "discounted_price"},
                    {data: "total_price"},
                    {
                        data: null,
                        defaultContent: "<button class='print btn btn-info btn-sm'><i class='fa fa-print'></i></button>"
                    },
                    {data: "id", visible: false},
                ],
                paging: false,
                ordering: false,
                searching: false,
                info: false
            });
            $('#table-invoice tbody').on('click', 'button', function () {
                var data = table_invoice.row($(this).parents('tr')).data();
                console.log(data.id)
            });
        })
    </script>
@endsection
