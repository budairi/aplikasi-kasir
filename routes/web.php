<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(array("prefix" => "product", "middleware" => 'verified'), function () {
    Route::get("/list", "ProductController@index")->name("product");
    Route::get("/list/json", "ProductController@list")->name("product.list.json");
    Route::get("/list/data", "ProductController@list_data")->name("product.list.data");
    Route::post("/new", "ProductController@new")->name("product.new");
    Route::get("/purchase", "ProductController@purchase")->name("product.purchase");
    Route::post("/purchase", "ProductController@purchase_data")->name("product.purchase.data");
    Route::get("/sales", "ProductController@sales")->name("product.sales");
    Route::post("/sales", "ProductController@sales_data")->name("product.sales.data");
    Route::post("/sales/cart", "ProductController@cart")->name("product.sales.cart");
    Route::post("/sales/cart/remove", "ProductController@cart_remove")->name("product.sales.cart.remove");
    Route::post("/invoice/new", "ProductController@new_invoice")->name("product.invoice.new");
    Route::post("/invoice/list", "ProductController@invoice_list")->name("product.invoice.data");
});
